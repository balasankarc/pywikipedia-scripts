#! /usr/bin/env python
#
# Copyright (c) 2013
#        Balasankar C <balasankarc@gnome.org>
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import gtk
import sys
import os
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.pdfpage import PDFPage
from pdfminer.converter import TextConverter
from pdfminer.cmapdb import CMapDB
from pdfminer.layout import LAParams
import wikipedia
import payyans
import pyPdf


class Base:

    index_file = ""
    pdf_file = ""
    map_file = ""

    def selectfile(self, widget):
        dialog = gtk.FileChooserDialog("Select Input File", None, gtk.FILE_CHOOSER_ACTION_OPEN, (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL, gtk.STOCK_OPEN, gtk.RESPONSE_OK))
        dialog.set_default_response(gtk.RESPONSE_OK)
        dialog.set_select_multiple(False)
        filter = gtk.FileFilter()
        filter.set_name("PDF Files")
        filter.add_pattern("*.pdf")
        filter.add_pattern("*.PDF")
        dialog.add_filter(filter)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.pdf_file = dialog.get_filenames()
        dialog.destroy()

    def get_text_from_pdf(self, i):
        '''Extracting Text from PDF File, one page at a time'''
        print "Inside function"
        debug = 0
        password = ''
        pagenos = set()
        pagenos.add(i)
        maxpages = 0
        imagewriter = None
        codec = 'utf-8'
        caching = True
        laparams = LAParams()
        PDFDocument.debug = debug
        PDFParser.debug = debug
        CMapDB.debug = debug
        PDFResourceManager.debug = debug
        PDFPageInterpreter.debug = debug
        PDFDevice.debug = debug
        rsrcmgr = PDFResourceManager(caching=caching)
        outfp = open('/tmp/temp.txt', 'w')
        device = TextConverter(rsrcmgr, outfp, codec=codec, laparams=laparams, imagewriter=imagewriter)
        fp = file(self.fname, 'rb')
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password, caching=caching, check_extractable=True):
            interpreter.process_page(page)
        fp.close()
        device.close()
        outfp.close()
        outfp = open('/tmp/temp.txt', 'r')
        unicode_text = outfp.read()
        outfp.close()
        return unicode_text

    def processfile(self, widget):
        self.index_file = self.select_index_file.get_text()
        self.map_file = self.select_map_file.get_active_text().split('.')[0]
        if self.index_file == "" or self.index_file == "Index Page (without namespace)":
            md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, "Please give the name of index file")
            md.run()
            md.destroy()
            return

        if self.pdf_file == "":
            md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, "Please Select PDF File")
            md.run()
            md.destroy()
            return

        if self.map_file == "Select Key Map File":
            md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, "Please Select KeyMap File")
            md.run()
            md.destroy()
            return

        self.fname = self.pdf_file[0]
        payyan_instance = payyans.getInstance()
        pdfreader = pyPdf.PdfFileReader(open(self.fname))
        siteFamily = 'wikisource'
        siteLangCode = 'ml'
        myNumber = pdfreader.getNumPages()
        resultPage = self.index_file
        wikiSite = wikipedia.Site(code=siteLangCode, fam=siteFamily)
        myResultPage = wikipedia.Page(site=wikiSite, title=resultPage)
        for i in range(0, myNumber):
            wikipedia.output("Page "+str(i+1)+" to be inserted")
            myResultPage = wikipedia.Page(site=wikiSite, title="Page:" + resultPage + "/" + str(i+1))
            myText = self.get_text_from_pdf(i)
            unicode_out = payyan_instance.ASCII2Unicode(myText.decode('utf-8'), self.map_file)
            if(myResultPage.exists()):
                continue
            else:
                print ""
                #myResultPage.append(unicode_out,comment=ur"ASCII2Unicode using Payyans")
        wikipedia.stopme()
        md = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO, gtk.BUTTONS_CLOSE, "Successful")
        md.run()
        md.destroy()
        return

    def window_close(self, widget):
        sys.exit(0)

    def __init__(self):
        window = gtk.Window()
        window.set_position(gtk.WIN_POS_CENTER)
        window.windowheight = 1000
        window.set_size_request(310, 300)
        window.show()
        fixed = gtk.Fixed()
        window.connect("destroy", self.window_close)
        self.select_index_file = gtk.Entry(1000)
        self.select_index_file.set_text("Index Page (without namespace)")
        self.select_input = gtk.Button("Choose Input PDF")
        self.select_map_file = gtk.combo_box_new_text()
        self.process = gtk.Button("Process the PDF")
        self.select_input.connect("clicked", self.selectfile)
        self.process.connect("clicked", self.processfile)
        self.select_input.set_size_request(250, 50)
        self.process.set_size_request(250, 50)
        self.select_index_file.set_size_request(250, 50)
        self.select_map_file.set_size_request(250, 50)
        fixed.put(self.select_input, 30, 30)
        fixed.put(self.select_index_file, 30, 90)
        fixed.put(self.select_map_file, 30, 150)
        fixed.put(self.process, 30, 210)
        map_list = os.listdir("/usr/local/lib/python2.7/dist-packages/payyans/maps")
        self.select_map_file.append_text("Select Key Map File")
        self.select_map_file.set_active(0)
        for maps in map_list:
            self.select_map_file.append_text(maps)
        window.add(fixed)
        window.show_all()

    def main(self):
        gtk.main()

if __name__ == "__main__":
    base = Base()
    base.main()
