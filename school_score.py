﻿#!/usr/bin/python
# -*- coding: utf-8 -*-


import wikipedia
#import pagegenerators
#import codecs
#import catlib

siteFamily = 'wikisource'
siteLangCode = 'ml'
inp = open("input.csv", "r")
listofschools = inp.read().split("\n")
#print listofschools
wikipedia.setLogfileStatus(True)
scorecard = {}
wikiSite = wikipedia.Site(code=siteLangCode, fam=siteFamily)
for entry in listofschools[:-1]:
    entry1 = entry.split(',')
    schoolcode = entry1[0]
    #print "Schoolcode = "+schoolcode
    index = entry1[1]
    print "Index = "+index
    startpage = int(entry1[2])
    #print "Start Page = "+str(startpage)
    endpage = int(entry1[3])
    #print "End Page = "+str(endpage)
    for i in range(startpage, endpage+1):
        mytitle = "Page:"+index+".pdf/"+str(i)
        #print mytitle
        currentpage = wikipedia.Page(site=wikiSite, title=mytitle)
        if currentpage.exists():
            noofchar=len(currentpage.get())
            if schoolcode in scorecard.keys():
                scorecard[schoolcode] += noofchar
            else:
                #print "Not in set"
                scorecard[schoolcode] = noofchar
    #print scorecard
out = '<link href="css/bootstrap.css" rel="stylesheet" /><body style="margin-left:10%;margin-right:10%;">    <h1 style="text-align:center;">        <img src="images/Wikisource-logo.png" width="100px" height="100px" /><br /><big>വിക്കിഗ്രന്ഥശാല    </big>   <br /> മത്സരത്തിന്റെ കണക്കുകൾ  </h1>'
out = out+'<table class="table table-bordered"><tr><th>സ്കൂളിന്റെ പേര് </th><th>താളുകളുടെ എണ്ണം  </th></tr>'
for key, value in sorted(scorecard.iteritems(), key=lambda(v, k): (k, v), reverse=True):
    out = out+"<tr><td>"+key.encode("UTF-8") + "</td><td>"+str(value) + "</td></tr>"
out = out+"</table>"
out = out+'<script src="https://code.jquery.com/jquery.js">  </script>  <script src="js/bootstrap.min.js">  </script><div class="footer-fixed-bottom" style="text-align:center" >Designed by <u><a href="http://ml.wikisource.org">Malayalam Wikisource</a></u></div> </body></html>'
outfile = open("/home/balasankarc/git/ProofreadingContest/result.html", "w")
outfile.write(out)
outfile.close()
